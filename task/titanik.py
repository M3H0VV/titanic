import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def prefix_fun(x):
        if 'Mr.' in x: return 'Mr.';
        elif 'Mrs.' in x: return 'Mrs.';
        elif 'Miss.' in x: return 'Miss.';

def get_filled():
    return_list = []
    df = get_titatic_dataframe()
    prefixes = ['Mr.','Mrs.','Miss.']
    df['Prefix'] = df["Name"].apply(prefix_fun)  
    age_median = df[["Age","Prefix"]].groupby(['Prefix']).median()
    for prefix in prefixes:
        median = int(age_median.loc[prefix])
        missing = int(sum(df[df['Prefix'] == prefix]['Age'].isna()))
        return_list.append((prefix,missing,median))
    return return_list

